import {GraphQLServer} from 'graphql-yoga'

// Type definations (Schema)
const typeDefs = `
  type Query {
    add(numbers: [Float]!): Float!
    greeting(name: String): String!
    grades: [Int!]!
    me: User!
    post: Post
  }

  type User {
    id: ID!
    name: String!
    email: String!
    age: Int
  }

  type Post {
    id: ID!
    title: String!
    body: String!
    published: Boolean!
  }
`

// Resolvers
const resolvers = {
  Query: {
    add(parent, {numbers}, ctx, info) {
      return numbers.filter(n => n).reduce((acc, n) => acc + n, 0)
    },
    greeting(parent, args, ctx, info) {
      const {name} = args
      return `Hello${name ? `, ${name}` : ''}`
    },
    grades(parent, args, ctx, info) {
      return [1, 2, 3, 4]
    },
    me() {
      return {
        id: 'asd',
        name: 'Mohamed',
        email: 'memo@123.com',
        age: 23,
      }
    },
    post() {
      return {
        id: 1,
        title: 'Cool',
        body: 'Graphql is coool',
        published: false,
      }
    },
  },
}

const server = new GraphQLServer({
  typeDefs,
  resolvers,
})

server.start(() => {
  console.log('server is up')
})
